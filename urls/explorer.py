from django.conf.urls import url

from uchikoma.console.views.console import Explorer

urlpatterns = [
    url(r'(?P<query_id>\d+)/$',         Explorer.QueryView.as_view(), name='query_detail'),
    url(r'(?P<query_id>\d+)/download$', Explorer.download_query, name='download_query'),
    url(r'(?P<query_id>\d+)/stream$',   Explorer.stream_query, name='stream_query'),
    url(r'download$',                   Explorer.download_from_sql, name='download_sql'),
    url(r'(?P<query>\d+)/email_csv$',   Explorer.email_csv_query, name='email_csv_query'),
    url(r'(?P<pk>\d+)/delete$',         Explorer.DeleteQueryView.as_view(), name='query_delete'),
    url(r'new/$',                       Explorer.CreateQueryView.as_view(), name='query_create'),
    url(r'play/$',                      Explorer.PlayQueryView.as_view(), name='explorer_playground'),
    url(r'schema/$',                    Explorer.schema, name='explorer_schema'),
    url(r'logs/$',                      Explorer.ListQueryLogView.as_view(), name='explorer_logs'),
    url(r'format/$',                    Explorer.format_sql, name='format_sql'),
    url(r'^$',                          Explorer.ListQueryView.as_view(), name='explorer_index'),
]
