from django.conf.urls import include, url

from gestalt.web.utils import Reactor

from uchikoma.console.views import console as Views

################################################################################

urlpatterns = Reactor.router.urlpatterns('console',
    url(r'^sandbox/$',                    Views.SandBox.homepage,   name='sandbox__homepage'),
    url(r'^sandbox/debugger$',            Views.SandBox.debugger,   name='sandbox__debugger'),
    url(r'^sandbox/cheatsheet$',          Views.SandBox.cheatsheet, name='sandbox__cheatsheet'),

    url(r'^shell$',                       Views.Tools.shell_cmd, name='shell'),

    #url(r'^$',                            Views.Homepage.as_view(), name='homepage'),
)

