from django.conf.urls import include, url

from uchikoma.console.views import collab as views

################################################################################

urlpatterns = [
    url(r'^projects$',                    views.projet__list, name='project__list'),
    url(r'^projects/(?P<owner>[^/]+)/$',  views.projet__view, name='project__view'),

    #url(r'^$',                            views.Homepage.as_view(), name='homepage'),
]

