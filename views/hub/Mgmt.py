#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

################################################################################

# @fqdn.route(r'^companies$', strategy='login')
def company_list(context):
    context.template = 'hub/rest/org/list.html'

    return {
        'listing': [entry for entry in Odoo.model('res.company').read([])],
    }

################################################################################

# @fqdn.route(r'^~(?P<narrow>.+)/$', strategy='login')
def company_view(context, narrow):
    context.template = 'hub/rest/org/view.html'

    qs = Odoo.model('res.company').browse('name like ' + narrow).read([])

    if len(qs):
        return {
            'company': qs[0],
        }

