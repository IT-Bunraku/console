#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

################################################################################

@Reactor.router.register_route('hub', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "hub/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

        yield Reactor.ui.Widget('visits', 'visits', title="Visits", heading="Based on a three months data", size=dict(sm=3,xs=6), flags=[
            dict(title="Total Traffic", value=24541,    icon='users',       color='green'),
            dict(title="Unique Users",  value=14778,    icon='bolt',        color='red'),
            dict(title="Revenue",       value=3583.18,  icon='plus-square', color='green'),
            dict(title="Total Sales",   value=59871.12, icon='user',        color='red'),
        ])

        yield Reactor.ui.Widget('traffic', 'traffic', title="Traffic Sources", heading="One month tracking", size=dict(sm=3,xs=6), fields=[
            dict(label="Source", header="source-col-header"),
            dict(label="Amount"),
            dict(label="Change"),
            dict(label="Percent.,%", header="hidden-xs"),
            dict(label="Target"),
            dict(label="Trend", header="chart-col-header hidden-xs"),
        ], rows=[
            
        ])

        yield Reactor.ui.Widget('news', 'news', title="Newsfeed", heading="all combined", size=dict(sm=3,xs=6), entries=[
            dict(
                sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                receiver='',
                when='4 min',
                content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
            ),
        ])

        yield Reactor.ui.Widget('chat', 'chat', title="Instant Talk", heading="with John Doe", size=dict(sm=3,xs=6), messages=[
            dict(
                sender=dict(full="Tikhon Laninga", mugshot='img/2.jpg'),
                receiver='',
                when='4 min',
                content="Hey Sam, how is it going? But I must explain to you how all this mistaken idea of denouncing of a pleasure and praising pain was born",
            ),
        ])

################################################################################

from . import Mgmt

#from . import Social

################################################################################

#urlpatterns = fqdn.urlpatters

