#-*- coding: utf-8 -*-

from uchikoma.console.shortcuts import *

################################################################################

@fqdn.route(r'^projects$', strategy='login')
def listing(context):
    context.template = 'console/rest/project/list.html'

    return {
        'listing': Project.objects.all(),
    }

################################################################################

@fqdn.route(r'^projects/(?P<narrow>.+)/$', strategy='login')
def overview(context, narrow):
    context.template = 'console/rest/project/view.html'

    resp = None

    resp = Project.objects.get(name=narrow)
    try:
        pass
    except:
        raise Http404()

    return {
        'prj': resp,
    }

#*******************************************************************************

@fqdn.route(r'^projects/(?P<narrow>.+)/setup$', strategy='login')
def settings(context, narrow):
    context.template = 'console/rest/project/tokens.html'

    resp = None

    try:
        resp = Project.objects.get(name=narrow)
    except:
        raise Http404()

    return {
        'prj': resp,
    }

#*******************************************************************************

@fqdn.route(r'^projects/(?P<narrow>.+)/delete$', strategy='login')
def delete(context, narrow):
    context.template = 'console/rest/project/delete.html'

    resp = None

    try:
        resp = Project.objects.get(name=narrow)
    except:
        raise Http404()

    return {
        'prj': resp,
    }

#*******************************************************************************

@fqdn.route(r'^projects/(?P<narrow>.+)/access$', strategy='login')
def manage_access(context, narrow):
    context.template = 'console/rest/project/access.html'

    resp = None

    try:
        resp = Project.objects.get(name=narrow)
    except:
        raise Http404()

    return {
        'prj': resp,
    }

#*******************************************************************************

@fqdn.route(r'^projects/(?P<narrow>.+)/tokens$', strategy='login')
def manage_tokens(context, narrow):
    context.template = 'console/rest/project/tokens.html'

    resp = None

    try:
        resp = Project.objects.get(name=narrow)
    except:
        raise Http404()

    return {
        'prj': resp,
    }

#*******************************************************************************

@fqdn.route(r'^projects/(?P<narrow>.+)/quota$', strategy='login')
def view_quota(context, narrow):
    context.template = 'console/rest/project/quota.html'

    resp = None

    try:
        resp = Project.objects.get(name=narrow)
    except:
        raise Http404()

    return {
        'prj': resp,
    }
