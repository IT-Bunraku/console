#-*- coding: utf-8 -*-

from uchikoma.console.shortcuts import *

################################################################################

@fqdn.route(r'^shell/$', strategy='login')
def listing(context):
    context.template = 'console/views/shell/home.html'

    return {'version': version}

################################################################################

@fqdn.route(r'^shell/(?P<narrow>.+)/?$', strategy='login')
def overview(context, narrow):
    context.template = 'console/views/shell/view.html'

    return {'version': version}
