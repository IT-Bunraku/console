#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *
from uchikoma.console.shortcuts import *

################################################################################

@Reactor.router.register_route('collab', r'^$', strategy='login')
class Homepage(Reactor.ui.Dashboard):
    template_name = "collab/views/homepage.html"

    def enrich(self, request, **context):
        context['primitives'] = enumerate_schemas(owner_id=request.user.pk)

        return context

    def populate(self, request, **context):
        for label,icon,stats in context['primitives']:
            yield Reactor.ui.Block('counter', label.lower(),
                title=label, icon=icon,
                size=dict(md=3, xs=6),
            value=stats)

################################################################################

@login_required
@render_to('rest/project/list.html')
def projet__list(request):
    return context(
        listing=[
            idn.__json__
            for idn in Identity.objects.all()
        ],
    )

#*******************************************************************************

@login_required
@render_to('rest/project/view.html')
def projet__view(request, nrw):
    idn = Identity.objects.get(alias=nrw)

    return context(idn.__json__)

################################################################################


