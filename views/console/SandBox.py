from uchikoma.console.shortcuts import *

################################################################################

@login_required
@render_to('console/views/sandbox/homepage.html')
def homepage(request):
    return context()

################################################################################

@login_required
@render_to('console/views/sandbox/debugger.html')
def debugger(request):
    return context()

################################################################################

@login_required
@render_to('console/views/sandbox/cheatsheet.html')
def cheatsheet(request):
    return context()

