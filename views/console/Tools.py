from uchikoma.console.shortcuts import *

from django.conf.urls import patterns
from django.contrib import admin
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.conf import settings
import subprocess

################################################################################

@login_required
@render_to('console/views/tools/search.html')
def searchbox(request):
    return context()

################################################################################

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

#*******************************************************************************

@login_required
def shell_cmd(request):
    try:
        v1 = request.is_secure() == settings.SECURE_CONSOLE
    except AttributeError:
        v1 = True
    try:
        v2 = get_client_ip(request) in settings.CONSOLE_WHITELIST
    except AttributeError:
        v2 = True
    except:
        print("CONSOLE_WHITELIST needs to be a list of ip addresses to be allowed access")
        v2 = True

    settings_variables = v1 and v2

    if request.user.is_superuser:
        if request.method=='POST':
            command = request.POST.get("command")
            if command:
                try:
                    data = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
                except subprocess.CalledProcessError as e:
                    data = e.output
                data = data.decode('utf-8')
                output = "%c(@olive)%" + data + "%c()"
            else:
                output = "%c(@orange)%Try `ls` to start with.%c()"
            return HttpResponse(output)
        elif settings_variables:
            context = {
                'STATIC_URL': settings.STATIC_URL
            }
            context.update(csrf(request))
            return render_to_response('console/views/tools/shell.html', RequestContext(request, context))
        else:
            return HttpResponse("Unauthorized.", status=403)

