from django.contrib import admin

from import_export.admin import ImportExportModelAdmin, ImportExportActionModelAdmin

from .models import *

from .actions import generate_report_action

################################################################################

class TeamAdmin(ImportExportActionModelAdmin):
    list_display = ('organization', 'name', 'title', 'created_on')
    list_filter  = ('organization', 'created_on')

admin.site.register(Team, TeamAdmin)

#*******************************************************************************

class ProjectAdmin(ImportExportActionModelAdmin):
    list_display = ('organization', 'name', 'title', 'created_on')
    list_filter  = ('organization', 'created_on', 'teams__name')

admin.site.register(Project, ProjectAdmin)

################################################################################

class QueryAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'created_by_user',)
    list_filter = ('title',)
    raw_id_fields = ('created_by_user',)
    
    actions = [generate_report_action()]

admin.site.register(Query, QueryAdmin)

