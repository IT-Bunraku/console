#-*- coding: utf-8 -*-

from uchikoma.connector.shortcuts import *

#*******************************************************************************

from tastypie.resources import ModelResource

################################################################################

@Reactor.router.register_rest(r'^organizations/', name='rest_organization')
class OrganizationView(ModelResource):
    class Meta:
        queryset = Organization.objects.all()
        resource_name = 'organization'
        #fields = ('url', 'username', 'email', 'is_staff')

#*******************************************************************************

@Reactor.router.register_rest(r'^teams/', name='rest_team')
class TeamView(ModelResource):
    class Meta:
        queryset = Team.objects.all()
        resource_name = 'team'
        #fields = ('url', 'username', 'email', 'is_staff')

################################################################################


