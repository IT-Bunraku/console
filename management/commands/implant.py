#!/usr/bin/env python

import os, sys, time, logging
import glob, json, yaml

from django.core.management.base import BaseCommand, CommandError

from chatterbot import ChatBot

################################################################################

while True:

#*******************************************************************************

class Command(BaseCommand):
    help = 'Run an chat-based implant'

    def add_arguments(self, parser):
        pass # parser.add_argument('poll_id', nargs='+', type=int)

    def handle(self, *args, **options):
        bot = ChatBot("Terminal",
            storage_adapter="chatterbot.adapters.storage.MongoDatabaseAdapter",
            logic_adapters=[
                "chatterbot.adapters.logic.MathematicalEvaluation",
                "chatterbot.adapters.logic.TimeLogicAdapter",
                "chatterbot.adapters.logic.ClosestMatchAdapter",
            ],
            input_adapter="chatterbot.adapters.input.TerminalAdapter",
            output_adapter="chatterbot.adapters.output.TerminalAdapter",
            database="chatterbot-database"
        )

        while True:
            try:
                bot_input = bot.get_response(None)
            except (KeyboardInterrupt, EOFError, SystemExit):
                break
            except:
                logging.exception('OOPS')
